# How to create a Vagrant Oracle Linux 7 Dev Server with Apache, Postgresql, and Django 2.1

> These instructions assume you have a directory $HOME/ProjectsRepository and have installed VirtualBox, Vagrant, Git-Bash, and the VScode editor on your PC.

## Open Git-Bash and add vagrant box

    $ vagrant box add --name ol77 https://yum.oracle.com/boxes/oraclelinux/ol77/ol77.box

## Create a CamelCased-named project directory

    $ mkdir /c/Users/username/ProjectsRepository/ProjectName

## Clone django-vagrant repository

    $ cd ~/ProjectsRepository/ProjectName
    $ git clone https://gitlab.com/bayermd/django-vagrant.git

## In django-vagrant directory, use sed to replace all instances of ProjectName and projectname with actual project name

    $ cd ~/ProjectsRepository/ProjectName/django-vagrant 
    $ grep -rl ProjectName . | xargs sed -i 's/ProjectName/ActualName/g'
    $ grep -rl projectname . | xargs sed -i 's/projectname/actualname/g'

## Continue following this README.md in VScode and initialize Vagrant Box

    $ code README.md 
    $ cd ~/ProjectsRepository/ProjectName 
    $ vagrant init ol77

## Replace generated Vagrantfile with that in django-vagrant

    $ cp -iv ~/ProjectsRepository/ProjectName/django-vagrant/Vagrantfile ~/ProjectsRepository/ProjectName/Vagrantfile

## Install vbguest plugin and vagrant up

    $ vagrant plugin install vagrant-vbguest
    $ vagrant up

## SSH in, set SELinux to permissive, logout, reload, and ssh back in

    $ vagrant ssh
    $ sudo emacs /etc/selinux/config #set to permissive
    $ logout
    $ vagrant reload #this will load guest additions and kernel modules
    $ vagrant ssh

> Note: vagrant uses .vagrant/machines/default/virtualbox/private_key. 
> If preferable, uncomment script section in Vagrantfile to inject ~/.ssh/id-rsa.pub into guest. 
> To troubleshoot ssh use command vagrant ssh -- -vvv. 
> ssh -p 2222 vagrant@127.0.0.1 also works. 
> You can also open VirtualBox GUI, click show to bring up console, and login there. 

## Configure Postgresql

    $ sudo su - postgres
    $ emacs /var/lib/pgsql/data/pg_hba.conf   #host all all 0.0.0.0/0 trust and change ident and peer to trust
    $ emacs /var/lib/pgsql/data/postgresql.conf   #listen_addresses = '*'
    $ exit
    $ sudo service postgresql.service restart

## Edit httpd.conf and copy django-vagrant/django.conf
    
    $ chmod 710 /home/vagrant
    $ sudo emacs /etc/httpd/conf/httpd.conf #Add ServerName localhost and change var/www/html directory to AllowOveride All
    $ sudo cp /vagrant/django-vagrant/django.conf /etc/httpd/conf.d/
    $ sudo apachectl configtest
    $ sudo apachectl restart
    
    # Open 127.0.0.1:8080 on host. Should see the default Apache page
    # Or test for errors
        $ httpd -t
        $ sudo tail -30 /var/log/httpd/error_log

## Install Django in venv

    $ cd /home/vagrant/ProjectName
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip3 install --upgrade pip
    $ pip3 install Django==2.1
    $ pip3 install mod_wsgi
    $ pip3 install psycopg2

## Setup the Django project and app
    $ django-admin.py startproject projectname
    $ sudo chown -R vagrant:apache ~/ProjectName/projectname
    $ sudo chmod -R 775 ~/ProjectName/projectname
    $ cd projectname
    # Change settings' directory name to config
        $ mv projectname config
    # Change "projectname.settings" to "config.settings" in manage.py
        $ emacs manage.py
    $ mkdir media static templates
    $ cp -iv /vagrant/django-vagrant/settings.py ~/ProjectName/projectname/config/
    $ cp -iv /vagrant/django-vagrant/wsgi.py ~/ProjectName/projectname/config/
    $ python manage.py startapp appname
    # Add app to settings.py
        $ emacs ./config/settings.py
    # Visit http://127.0.0.1:8080 on host to see default Django page

## Continue Django development and git init in ~/ProjectName/projectname

    $ ./manage.py migrate
    $ ./manage.py createsuperuser
    $ ./manage.py collectstatic
    # etc
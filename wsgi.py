"""
WSGI config for ProjectName 

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/

"""

import os, sys

sys.path.append('/home/vagrant/ProjectName/projectname')

sys.path.append('/home/vagrant/ProjectName/venv/lib/python3.6/site-packages')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


# -*- mode: ruby -*-
# vi: set ft=ruby :
  
Vagrant.configure("2") do |config|
  config.vm.box = "ol77"

  # Create a forwarded port mapping which allows access to a specific port
  config.vm.network "forwarded_port", guest: 8000, host: 8000
  config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a public network
  #config.vm.network "public_network"

  # Disable shared folders
  #config.vm.synced_folder ".", "/vagrant", disabled: true
  
  # Share an additional folder
  #config.vm.synced_folder "../data", "/vagrant_data"

  # Use host id_rsa on guest as authorized_keys
  #config.vm.provision "shell" do |s|
  #  ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
  #  s.inline = <<-SHELL
  #    mkdir ~/.ssh && touch authorized_keys
  #    echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
  #    chmod 600 /home/vagrant/.ssh/authorized_keys
  #    echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
  #  SHELL
  #end

  config.vm.provision "shell", inline: <<-SHELL
    
    echo "----Install Fedora EPEL Repository----"
    sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo yum update -y
    
    echo "----Begin HTTPD Installation and configuration----"
    sudo yum install -y httpd
    sudo yum install -y httpd-devel
    sudo service httpd start
    sudo chkconfig httpd on
    sudo chown -R apache: /var/www/html
    sudo usermod -a -G vagrant apache
    sudo usermod -a -G apache vagrant
    sudo chmod -R 775 /var/www/html
    sudo apachectl restart
    
    echo "----Begin Python Installation----"
    sudo yum install -y python3 python3-devel python3-setuptools
    sudo yum groupinstall -y "Development Tools"
    # Install mod_wsgi.so from source
    mkdir src 
    cd src
    wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.6.5.tar.gz
    tar -xzvf 4.6.5.tar.gz
    cd mod_wsgi-4.6.5
    ./configure --with-python=/usr/bin/python3
    make && make install
    echo "LoadModule wsgi_module modules/mod_wsgi.so" >> /etc/httpd/conf.modules.d/00-base.conf
    
    echo "----Begin Postgesql Installation----"
    sudo yum install -y postgresql
    sudo yum install -y postgresql-server
    sudo yum install -y postgresql-devel
    sudo yum install -y postgresql-contrib
    export PGSETUP_INITDB_OPTIONS="--encoding=UTF-8 --no-locale"
    sudo service postgresql initdb
    sudo service postgresql start
    sudo chkconfig postgresql on
    sudo -i -u postgres psql -c "CREATE DATABASE projectname"
    sudo -i -u postgres psql -c "CREATE USER vagrant WITH PASSWORD 'vagrant'"
    sudo -i -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE projectname TO vagrant"
    
    echo "----Begin Additional Tools and Django Installation----"
    sudo yum install -y git
    sudo yum install -y emacs
    sudo yum install -y screen
    mkdir /home/vagrant/ProjectName
    sudo chown -R vagrant: /home/vagrant/ProjectName
    sudo chmod -R 775 /home/vagrant/ProjectName
    
    echo "---- All set. vagrant ssh to login. Password = vagrant ----"
  SHELL
end